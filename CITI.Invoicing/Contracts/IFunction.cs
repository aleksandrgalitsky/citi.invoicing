﻿using Microsoft.Extensions.Logging;

namespace CITI.Invoicing.Function.Contracts
{
    public interface IFunction
    {
        ILogger Log { get; set; }
    }
}
