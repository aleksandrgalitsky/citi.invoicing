using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CITI.Invoicing.Function.Contracts;
using CITI.Invoicing.Function.Extensions;
using static QRCoder.PayloadGenerator;
using QRCoder;
using System.Drawing;

namespace CITI.Invoicing
{
    public static class HttpFunctions
    {
        [FunctionName("RenderInvoice")]
        public static async Task<IActionResult> RenderInvoice(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "render/{test}")] HttpRequest req,
            string test, ExecutionContext context, ILogger log)
        {
            log.LogInformation("");
            
            var function = context.GetFunction<IInvoicingFunction>(log);
            var result = await function.RenderInvoice(test);

            return new OkObjectResult(result);
        }

        [FunctionName("Test")]
        public static async Task<IActionResult> Test(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "test/{test}")] HttpRequest req,
            string test, ExecutionContext context, ILogger log)
        {
            log.LogInformation("");
            
            /*SwissQrCode.Contact contactGeneral = new SwissQrCode.Contact("John Doe", "3003", "Bern", "CH", "Parlamentsgebäude", "1");
            SwissQrCode.Iban iban = new SwissQrCode.Iban("CH2609000000857666015", PayloadGenerator.SwissQrCode.Iban.IbanType.QrIban);
            SwissQrCode.Reference reference = new SwissQrCode.Reference(SwissQrCode.Reference.ReferenceType.QRR, "990005000000000320071012303", SwissQrCode.Reference.ReferenceTextType.QrReference);
            SwissQrCode.AdditionalInformation additionalInformation = new SwissQrCode.AdditionalInformation("This is my unstructured message.", "Some bill information here...");
            SwissQrCode.Currency currency = SwissQrCode.Currency.CHF;
            decimal amount = 100.25m;

            SwissQrCode generator = new SwissQrCode(iban, currency, contactGeneral, reference, additionalInformation, null, amount, null, null);
            string payload = generator.ToString();

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(payload, QRCodeGenerator.ECCLevel.M);
            QRCode qrCode = new QRCode(qrCodeData);

            var qrCodeAsBitmap = qrCode.GetGraphic(20, Color.Black, Color.White);
            */
            return new OkObjectResult("");
        }
    }
}
