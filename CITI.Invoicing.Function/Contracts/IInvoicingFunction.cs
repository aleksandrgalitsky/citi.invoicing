﻿using CITI.Invoicing.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Invoicing.Function.Contracts
{
    public interface IInvoicingFunction : IFunction
    {
        Task<byte[]> RenderInvoice(RenderModel model);
    }
}
