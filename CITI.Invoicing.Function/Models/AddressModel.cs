﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Invoicing.Function.Models
{
    public class AddressModel
    {
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        public string PostalCode { get; set; }
        public string Town { get; set; }
        public string CountryCode { get; set; }
    }
}
