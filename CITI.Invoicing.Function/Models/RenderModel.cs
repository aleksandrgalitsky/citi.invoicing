﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace CITI.Invoicing.Function.Models
{
    public class RenderModel
    {
        public string Template { get; set; }
        public BillModel Bill { get; set; }

        public JObject Model { get; set; }
    }
}
