﻿using CITI.Invoicing.Function.Contracts;
using CITI.Invoicing.Models;
using CITI.Invoicing.Services.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Invoicing.Function.Functions
{
    public class InvoicingFunction : IInvoicingFunction
    {
        private IInvoicingService _invoicingService;

        public ILogger Log { get; set; }

        public InvoicingFunction(IInvoicingService invoicingService)
        {
            _invoicingService = invoicingService;
        }

        public async Task<byte[]> RenderInvoice(RenderModel model)
        {
            var result = await _invoicingService.RenderInvoice(model);
            return result;
        }
    }
}
