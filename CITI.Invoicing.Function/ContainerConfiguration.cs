﻿using CITI.Invoicing.Function.Contracts;
using CITI.Invoicing.Function.Functions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CITI.Invoicing.Function
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            Services.ContainerConfiguration.RegisterTypes(servicesCollection, config);

            servicesCollection.AddTransient<IInvoicingFunction, InvoicingFunction>();
        }
    }
}
