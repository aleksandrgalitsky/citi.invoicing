﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using CITI.Invoicing.Function.Contracts;

namespace CITI.Invoicing.Function.Extensions
{
    public static class ExecutionContextExtensions
    {
        private static IServiceProvider _serviceProvider;
        private static readonly object _lock = new object();

        public static T GetFunction<T>(this ExecutionContext context, ILogger logger)
            where T : IFunction
        {
            lock (_lock)
            {
                if (_serviceProvider == null)
                {
                    var services = new ServiceCollection();

                    var config = new ConfigurationBuilder()
                        .SetBasePath(context.FunctionAppDirectory)
                        .AddJsonFile("settings.json", optional: false, reloadOnChange: true)
                        .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                        .Build();

                    services.AddSingleton<IConfiguration>(config);

                    ContainerConfiguration.RegisterTypes(services, config);
                    _serviceProvider = services.BuildServiceProvider();
                }
            }

            var serviceProvider = _serviceProvider.CreateScope().ServiceProvider;

            var function = serviceProvider.GetRequiredService<T>();
            function.Log = logger;
            return function;
        }

        public static string GetUrl()
        {
            return Environment.GetEnvironmentVariable("Url");
        }
    }
}
