using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CITI.Invoicing.Function.Extensions;
using CITI.Invoicing.Function.Contracts;
using CITI.Invoicing.Models;

namespace CITI.Invoicing.Function
{
    public static class HttpFunction
    {
        [FunctionName("RenderInvoice")]
        public static async Task<IActionResult> RenderInvoice(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "render")]
            [FromBody] string model, ExecutionContext context, ILogger log)
        {
            log.LogInformation("Start");
            
            var render = JsonConvert.DeserializeObject<RenderModel>(model);
            var function = context.GetFunction<IInvoicingFunction>(log);
            var result = await function.RenderInvoice(render);
            
            return new FileContentResult(result, "application/pdf");
        }
    }
}
