﻿using CITI.Invoicing.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Invoicing.Services.Contracts
{
    public interface IInvoicingService
    {
        Task<byte[]> RenderInvoice(RenderModel model);
    }
}
