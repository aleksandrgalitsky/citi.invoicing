﻿using CITI.Invoicing.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CITI.Invoicing.Services
{
    public static class ContainerConfiguration
    {
        public static void RegisterTypes(IServiceCollection servicesCollection, IConfiguration config)
        {
            servicesCollection.AddScoped<IInvoicingService, InvoicingService>();
        }
    }
}
