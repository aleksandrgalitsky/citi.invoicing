﻿using CITI.Invoicing.Models;
using CITI.Invoicing.Services.Contracts;
using Codecrete.SwissQRBill.Generator;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;
using System;
using System.Drawing;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace CITI.Invoicing.Services
{
    public class InvoicingService : IInvoicingService
    {
        public InvoicingService()
        {

        }

        private string GetQR(BillModel model)
        {
            var billFormat = new BillFormat();
            billFormat.OutputSize = OutputSize.QrCodeOnly;

            Bill bill = new Bill
            {
                Format = billFormat,
                Account = model.Account,
                Creditor = new Address
                {
                    Name = model.Creditor.Name,
                    AddressLine1 = model.Creditor.AddressLine1,
                    AddressLine2 = model.Creditor.AddressLine2,
                    CountryCode = model.Creditor.CountryCode
                },
                Amount = model.Amount,
                Currency = model.Currency,
                Debtor = new Address
                {
                    Name = model.Debtor.Name,
                    AddressLine1 = model.Debtor.AddressLine1,
                    AddressLine2 = model.Debtor.AddressLine2,
                    CountryCode = model.Debtor.CountryCode
                },
                Reference = model.Reference,
                UnstructuredMessage = model.UnstructuredMessage
            };

            byte[] svg = QRBill.Generate(bill);

            return System.Text.Encoding.Default.GetString(svg);
        }

        private string GetHtml(string qr, RenderModel model)
        {
            string template = model.Template;
            dynamic renderModel = model.Model;

            var config = new TemplateServiceConfiguration();
            config.Language = RazorEngine.Language.CSharp;
            config.EncodedStringFactory = new RawStringFactory();
            config.EncodedStringFactory = new HtmlEncodedStringFactory();
            config.CachingProvider = new DefaultCachingProvider();
            var service = RazorEngineService.Create(config);
            Engine.Razor = service;

            template = template.Replace("{qr}", $"{qr}");

            var result = Engine.Razor.RunCompile(template, "", null, renderModel as object);

            return result;
        }

        public async Task<byte[]> RenderInvoice(RenderModel model)
        {
            string qr = GetQR(model.Bill);
            string html = GetHtml(qr, model);

            byte[] pdf = OpenHtmlToPdf.Pdf.From(html).Content();

            return pdf;
        }
    }
}
