using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CITI.Invoicing.Function.Tests
{
    [TestClass]
    public class FunctionUnitTest
    {
        private MailingDataContextManager _dataContextManager;

        public FunctionUnitTest()
        {

            var services = new ServiceCollection();
            var config = new ConfigurationBuilder()
                .AddJsonFile("local.settings.json")
                .Build();

            services.AddLogging(options => options.AddDebug());

            ContainerConfiguration.RegisterTypes(services, config);
            var serviceProvider = services.BuildServiceProvider();
            _dataContextManager = serviceProvider.GetRequiredService<MailingDataContextManager>();
        }

        [TestMethod]
        public void TestMethod1()
        {
        }
    }
}
