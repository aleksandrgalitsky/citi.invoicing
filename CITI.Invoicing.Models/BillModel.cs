﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CITI.Invoicing.Models
{
    public class BillModel
    {
        public string Account { get; set; }
        public string BillInformation { get; set; }
        public string UnstructuredMessage { get; set; }
        public decimal? Amount { get; set; }
        public string Currency { get; set; }
        public AddressModel Creditor { get; set; }
        public string Reference { get; set; }
        public AddressModel Debtor { get; set; }
    }
}
